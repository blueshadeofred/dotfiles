#!/bin/bash

###############################INFO################################
#								  #
# Post-install-script for manjaro community i3wm minimal edition. #
#    Intends to clean up, remove, and apply good standards.    	  #
#								  #
#    !BE SURE TO STAY CONNECTED TO THE INTERNET WHEN RUNNING!	  #
#								  #
#    Author: TBA						  #
#								  #
###################################################################

##Delete un-needed files and directories in $HOME
echo "Deleting un-needed F's and D's in your home directory"

rm -rfv $HOME/Desktop $HOME/.moc $HOME/Music $HOME/Public $HOME/Templates $HOME/Videos $HOME/.local/share/moc && rm -v $HOME/.lesshst $HOME/.zhistory $HOME/.local/share/applications/userapp-Thunderbird.desktop &&

##Systemctl stuff
sudo systemctl disable ModemManager.service avahi-daemon.service avahi-daemon.socket &&
sudo systemctl mask avahi-daemon.service avahi-daemon.socket &&

##Activate UFW
sudo ufw default deny &&
sudo ufw deny ssh &&
sudo ufw deny vnc &&
sudo ufw deny telnet &&
sudo systemctl enable ufw &&
sudo systemctl start ufw &&
sudo ufw enable &&

##Install programs
sudo pacman -S bleachbit feh &&

##Uninstall programs
sudo pacman -Rns ncdu modemmanager dfc moc &&

##Copy dotfiles
#curl blalblablabla

##Exit
#Ask if want reboot Y/n
echo "Done"
sleep 2
exit 0
